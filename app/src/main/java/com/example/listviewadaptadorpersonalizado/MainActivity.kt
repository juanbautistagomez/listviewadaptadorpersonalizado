package com.example.listviewadaptadorpersonalizado

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ListView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        var listdat: ListView = findViewById(R.id.list_item)
        var dato: ArrayList<Producto> = ArrayList()

        dato.add(Producto("Pepsi",R.drawable.ic_launcher_background,"$15.67"))
        dato.add(Producto("Cocacola",R.drawable.ic_launcher_background,"$15.67"))
        dato.add(Producto("Bigcola",R.drawable.ic_launcher_background,"$15.67"))
        dato.add(Producto("poweride",R.drawable.ic_launcher_background,"$15.67"))
        dato.add(Producto("Jugos", R.drawable.ic_launcher_background,"$15.67"))
        dato.add(Producto("Cocacola",R.drawable.ic_launcher_background,"$15.67"))
        dato.add(Producto("Bigcola",R.drawable.ic_launcher_background,"$15.67"))
        dato.add(Producto("poweride",R.drawable.ic_launcher_background,"$15.67"))
        dato.add(Producto("Cocacola",R.drawable.ic_launcher_background,"$15.67"))
        dato.add(Producto("Bigcola",R.drawable.ic_launcher_background,"$15.67"))
        dato.add(Producto("poweride",R.drawable.ic_launcher_background,"$15.67"))
        dato.add(Producto("Cocacola",R.drawable.ic_launcher_background,"$15.67"))
        dato.add(Producto("Bigcola",R.drawable.ic_launcher_background,"$15.67"))
        dato.add(Producto("poweride",R.drawable.ic_launcher_background,"$15.67"))
        dato.add(Producto("Cocacola",R.drawable.ic_launcher_background,"$15.67"))
        dato.add(Producto("Bigcola",R.drawable.ic_launcher_background,"$15.67"))
        dato.add(Producto("poweride",R.drawable.ic_launcher_background,"$15.67"))


        val adaptador = Adaptador_listView(this,dato)
        listdat.adapter = adaptador
    }
}