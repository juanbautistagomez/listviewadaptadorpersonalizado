package com.example.listviewadaptadorpersonalizado

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView

class Adaptador_listView(var context: Context, items: ArrayList<Producto>) : BaseAdapter() {
    var items: ArrayList<Producto>? = null

    init {
        this.items = items
    }

    override fun getView(p0: Int, converview: View?, parent: ViewGroup?): View {

        var holder: ViewHolder? = null
        var vista: View? = converview

        if (vista == null) {
            vista = LayoutInflater.from(context).inflate(R.layout.resource_adapter, null)
            holder = ViewHolder(vista)
            vista.tag = holder
        } else {
            holder = vista.tag as? ViewHolder
        }

        var item = getItem(p0) as Producto
        holder?.nombre?.text = item.nombre
        holder?.icon?.setImageResource(item.icon)
        holder?.precio?.text = item.precio

        return vista!!
    }

    override fun getItem(p0: Int): Any {
        return items?.get(p0)!!
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return items?.count()!!
    }

    private class ViewHolder(vista: View) {
        var nombre: TextView? = null
        var icon: ImageView? = null
        var precio: TextView? = null

        init {
            nombre = vista.findViewById(R.id.nombre)
            icon = vista.findViewById(R.id.image)
            precio = vista.findViewById(R.id.precio)
        }

    }
}