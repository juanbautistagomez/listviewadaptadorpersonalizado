# ListView con adaptador personalizado #

Un objeto Adapter actúa como un puente entre AdapterViewlos datos subyacentes y los de esa vista. El adaptador proporciona acceso a los elementos de datos. El adaptador también es responsable de hacer un Viewpara cada elemento del conjunto de datos.


### Metodos públicos

### getAutofillOptions

public CharSequence [] getAutofillOptions ()
Obtiene una representación de cadena de los datos del adaptador que puede ayudar a completar AutofillServiceautomáticamente la vista respaldada por el adaptador.

Solo debe configurarse (es decir, no nullsi los valores no representan PII (información de identificación personal: datos confidenciales como direcciones de correo electrónico, números de tarjetas de crédito, contraseñas, etc.). Por ejemplo, está bien devolver una lista de nombres de meses, pero no una lista de nombres de usuario. Una buena regla general es que si los datos del adaptador provienen de recursos estáticos, dichos datos no son PII; consulte ViewStructure.setDataIsSensitive(boolean)para obtener más información.

### getCount

public abstract int getCount ()
Cuántos elementos hay en el conjunto de datos representado por este adaptador.

### getItem

getItem de objeto abstracto público (posición int)
Obtenga el elemento de datos asociado con la posición especificada en el conjunto de datos.

Parámetros
position	int: Posición del elemento cuyos datos queremos dentro del conjunto de datos del adaptador.
Devoluciones
Object	Los datos en la posición especificada.

### getItemId

getItemId largo público abstracto (posición int)
Obtenga el ID de fila asociado con la posición especificada en la lista.

Parámetros
position	int: La posición del elemento dentro del conjunto de datos del adaptador cuya identificación de fila queremos.
Devoluciones
long	La identificación del artículo en la posición especificada.

### getItemViewType

public abstract int getItemViewType (int posición)
Obtenga el tipo de vista que se creará getView(int, View, ViewGroup)para el elemento especificado.

Parámetros
position	int: La posición del elemento dentro del conjunto de datos del adaptador cuyo tipo de vista queremos.
Devoluciones
int	Un número entero que representa el tipo de vista. Dos vistas deben compartir el mismo tipo si una se puede convertir a la otra en . Nota: Los números enteros deben estar en el rango de 0 a -1. También se pueden devolver.getView(int, View, ViewGroup)getViewTypeCount()IGNORE_ITEM_VIEW_TYPE


### getView

                 vista convertView, 
                 ViewGroup padre)
Obtenga una vista que muestre los datos en la posición especificada en el conjunto de datos. Puede crear una vista manualmente o inflarla desde un archivo de diseño XML. Cuando la vista está inflada, la vista principal (GridView, ListView ...) aplicará los parámetros de diseño predeterminados a menos que utilice LayoutInflater.inflate(int, android.view.ViewGroup, boolean) para especificar una vista raíz y para evitar la conexión a la raíz.

Parámetros
position	int: La posición del elemento dentro del conjunto de datos del adaptador del elemento cuya vista queremos.
convertView	View: La vista anterior para reutilizar, si es posible. Nota: debe verificar que esta vista no sea nula y de un tipo apropiado antes de usarla. Si no es posible convertir esta vista para mostrar los datos correctos, este método puede crear una nueva vista. Las listas heterogéneas pueden especificar su número de tipos de vista, de modo que esta Vista sea siempre del tipo correcto (ver getViewTypeCount()y ).getItemViewType(int)
parent	ViewGroup: El padre al que eventualmente se adjuntará esta vista
Devoluciones
View	Una vista correspondiente a los datos en la posición especificada.

### getViewTypeCount

public abstract int getViewTypeCount ()
Devuelve el número de tipos de vistas que creará getView(int, View, ViewGroup). Cada tipo representa un conjunto de vistas que se pueden convertir getView(int, View, ViewGroup). Si el adaptador siempre devuelve el mismo tipo de Vista para todos los elementos, este método debería devolver 1.

Este método solo se llamará cuando el adaptador esté configurado en AdapterView.

Devoluciones
int	La cantidad de tipos de vistas que creará este adaptador

Asi quedaria usando un adaptador personalizado para un listview podes revisar el codigo para la guia

![picture](Screenshot_20200831-151254.png)
